package Task05.test;
import static org.junit.Assert.*;
import org.junit.Test;
import Task02.Item2d;
import Task03.src.ViewResult;
/** Тестирование класса
* ChangeItemCommand
* @author xone
* @version 4.0
* @see ChangeItemCommand
*/
public class MainTest {
/** Проверка метода {@linkplain ChangeItemCommand#execute()} */
@Test
public void testExecute() {
ChangeItemCommand cmd = new ChangeItemCommand();
cmd.setItem(new Item2d());
double x, y, offset;
for (int ctr = 0; ctr < 1000; ctr++) {
cmd.getItem().setXY(x = Math.random() * 100.0, y = Math.random() * 100.0);
cmd.setOffset(offset = Math.random() * 100.0);
cmd.execute();
assertEquals(x, cmd.getItem().getX(), .1e-10);
assertEquals(y * offset, cmd.getItem().getY(), .1e-10);
}
}
/** Проверка класса {@linkplain ChangeConsoleCommand} */
@Test
public void testChangeConsoleCommand() {
ChangeConsoleCommand cmd = new ChangeConsoleCommand(new ViewResult());
cmd.getView().viewInit();
cmd.execute();
assertEquals("'c'hange", cmd.toString());
assertEquals('c', cmd.getKey());
}
}