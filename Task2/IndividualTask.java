import java.util.*;
 
class Point {
    private final double x;
    private final double y;
 
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
 
    public double getX() {
        return x;
    }
 
    public double getY() {
        return y;
    }
 
    public double getDistanceTo(final Point p) {
        return Math.sqrt(Math.pow(getX() - p.getX(), 2.) + Math.pow(getY() - p.getY(), 2.0));
    }
 
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
 
class Figure {
    private final List<Point> points;
 
    private Figure(Collection<Point> points) {
        this.points = new ArrayList<>(points);
    }
 
    public List<Point> getPoints() {
        return Collections.unmodifiableList(points);
    }
 
    public double getPerimeter() {
        int numberOfPoints = points.size();
        if (numberOfPoints <= 1) {
            return 0;
        } else if (numberOfPoints == 2) {
            return points.get(0).getDistanceTo(points.get(1));
        } else {
            double accumulator = 0;
            for (int i = 0; i < numberOfPoints; ++i) {
                accumulator += points.get(i).getDistanceTo(points.get((i + 1) % numberOfPoints));
            }
            return accumulator;
        }
    }
 
    public static Figure createRectangle(double height, double width) {
        List<Point> points = Arrays.asList(
            new Point(0, 0),
            new Point(0, height),
            new Point(width, height),
            new Point(width, 0)
 
        );
        return new Figure(points);
    }
 
    public static Figure createIsoscelesTriangle(double height, double base) {
        List<Point> points = Arrays.asList(
            new Point(0, 0),
            new Point(base, 0),
            new Point(base / 2, height)
        );
        return new Figure(points);
    }
 
    @Override
    public String toString() {
        return "Figure{" +
                "points=" + points +
                ", perimeter=" + getPerimeter() +
                '}';
    }
}
 
public class Example {
    final static long m1  = 0x5555555555555555L; //binary: 0101...
    final static long m2  = 0x3333333333333333L; //binary: 00110011..
    final static long m4  = 0x0f0f0f0f0f0f0f0fL; //binary:  4 zeros,  4 ones ...
    final static long h01 = 0x0101010101010101L; //the sum of 256 to the power of 0,1,2,3...
 
 
    static long countBits(long x) {
        x -= (x >> 1) & m1;             //put count of each 2 bits into those 2 bits
        x = (x & m2) + ((x >> 2) & m2); //put count of each 4 bits into those 4 bits
        x = (x + (x >> 4)) & m4;        //put count of each 8 bits into those 8 bits
        return (x * h01)>>56;           //returns left 8 bits of x + (x<<8) + (x<<16) + (x<<24) + ...
    }
 
    public static void main(String... args) {
        Figure triangle = Figure.createIsoscelesTriangle(2, 4);
        Figure rectangle = Figure.createRectangle(2, 4);
 
        double totalPerimeter = rectangle.getPerimeter() + triangle.getPerimeter();
        long numberOfBits = countBits((long)totalPerimeter);
 
        System.out.println(triangle);
        System.out.println(rectangle);
        System.out.println("Total perimeter: " + totalPerimeter);
        System.out.println("Number of bits: " + numberOfBits);
    }
}